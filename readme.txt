=== FB Messenger Bot for WooCommerce ===
Contributors: pigeonhut
Tags: WooCommerce messenger, Facebook messenger
Requires at least: 4.5
Tested up to: 4.7.2
Stable tag: 1.1.2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Automatically message from your Facebook page to clients, either from WooCommerce or Gravity Forms.

== Description ==

Automatically message your clients from your Facebook page, either from WooCommerce or Gravity Forms. The plugin creates a "send to facebook" button at
the end of the WooCommerce Sales process or on the Gravity Forms thank you page. (see Screenshots)


== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/wpmessengerbot` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. On the left menu, you will now see WP Messenger Bot, you can add all the settings there.


== Frequently Asked Questions ==
How do I connect to Facebook
 	Create a FB Developer App at https://developers.facebook.com/apps/
	 	1.1 Save your new App ID on the Plugins Settings Page

On FB Developer Section, please fill in the following items:
	Add "Messenger" as a Product
	Get the Page Access Token
		Go to the "Token Generation" section on the Products->Messenger Screen.
		Select the Facebook Page you want to use for the app
		Paste the newly generated Access Token into your Access Token section in the WordPress plugin settings

Add webhooks which allow authentication between the FB page and the Website
On your Facebook App, goto -- Add Product -> Webhooks ->
	Add a Subscription: New Subsciption
	Fill out the Verify Token which can be found in the Plugin settings.
	Add the following Subscription Fields: message_delivery, messages, message_account_linking, message_optins, messaging_postbacks
	Click "Verify and Save"

How do I enable the Gravity forms hooks ?
	preface your form title with mbot_ for example if your form title was "contact us" it would not be "mbot_contact us" this will automatically tell Gravity
	forms to open a link to the users FB profile and message them once they authenticate

How do I activate WooCommerce messaging ?
	As soon as your FB Developer App is approved, once a user authenticates, they will automatically be sent the order updates and notes that you add to the order.


Why are the messenger checkbox and send to messenger button not displayed?
Please wait for your App to be approved by the Facebook Development team

== Screenshots ==

1. /assets/facebook-review.png
2. /assets/google-review.png
3. /assets/woocommerce-review.png
4. /assets/testimonial-review.png
5. /assets/all-reviews-review.png

== Changelog ==
= 1.1.2 =
Updated FAQ to reflect the correct FB App creation procedure


= 1.1.1 =
Initial public release

== Upgrade Notice ==

= 1.0 =
nothing yet
