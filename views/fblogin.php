 <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId: "<?php echo $settings['app_id']; ?>",
        xfbml: true,
        version: "v2.6"
      });

      FB.Event.subscribe('send_to_messenger', function(e) {
         // callback for events triggered by the plugin
       
       });

     };

     // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  </script>

<div class="fb-send-to-messenger" 
  messenger_app_id="<?php echo $settings['app_id']; ?>" 
  page_id="<?php echo $settings['page_id']; ?>" 
  data-ref="<?php echo $session_id; ?>" 
  color="blue" 
  size="standard">
</div>    
